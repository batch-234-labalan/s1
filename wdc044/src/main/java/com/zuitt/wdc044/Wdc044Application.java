package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
//annotation - under the hood codes to be used
@RestController
// handles the endpoint for web requests
public class Wdc044Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}

	//@RequestParam - annotation that tells spring to expect a name value in the request but if its not there, a default world will be used instead
	@GetMapping("/hello")
	//Getmapping tells spring to use this method when a GET request is received
	public String hello(@RequestParam(value = "name", defaultValue = "world") String name){

		return String.format("Hello %s", name);
		// %s - any type but the return is a String
		// %c - character and the output is a unicode character
		// %b - any type but the output is value
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "Jane") String name){

		return String.format("Hi %s", name);
	}

	@GetMapping("/nameage")

	public String nameage(@RequestParam(value = "user", defaultValue = "user") String user, @RequestParam(value = "age", defaultValue = "0") String age){

		return String.format("Hello %s! Your age is %s", user,age);
	}



}
